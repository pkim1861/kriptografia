"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this placeholder text with a description of this module.
"""
import string

import utils


#################
# CAESAR CIPHER #
#################

def encrypt_caesar(plaintext):
    """Encrypt a plaintext using a Caesar cipher.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    ciphertext = ""
    ABC = string.ascii_uppercase
    abc = string.ascii_lowercase

    dictionary = {}
    for c in ABC:
        dictionary[c] = ABC[(ABC.find(c) + 3) % 26]

    for c in plaintext:
        if c in ABC:
            ciphertext += dictionary[c]
        elif c in abc:
            return 'The input shouldn\'t contain lower-cased letters!'
        else:
            ciphertext += c

    return ciphertext


print(encrypt_caesar("Fa1RST P0ST"))


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.

    ABC = string.ascii_uppercase

    dictionary = {}
    for c in ABC:
        dictionary[c] = ABC[(ABC.find(c) - 3) % 26]

    plaintext = ""
    ABC = string.ascii_uppercase
    abc = string.ascii_lowercase

    for c in ciphertext:
        if c in ABC:
            plaintext += dictionary[c]
        elif c in abc:
            return 'The input shouldn\'t contain lower-cased letters!'
        else:
            plaintext += c

    return plaintext


print(decrypt_caesar("I1UVW S0VW"))


###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.

    ABC = string.ascii_uppercase
    abc = string.ascii_lowercase
    ciphertext = ""
    i = 0
    for c in plaintext:
        if c in ABC:
            if i == len(keyword):
                i = 0
            if (ord(c) + ord(keyword[i]) - ord('A')) <= ord('Z'):
                ciphertext = ciphertext + chr(ord(c) + ord(keyword[i]) - ord('A'))
            else:
                ciphertext = ciphertext + chr(ord(c) + ord(keyword[i]) - ord('Z') - 1)
            i += 1
        elif c in abc:
            return 'The input shouldn\'t contain lower-cased letters'
        else:
            ciphertext += c

    return ciphertext


print(encrypt_vigenere("ATTACKATDAWN", "LEMON"))


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.

    ABC = string.ascii_uppercase
    abc = string.ascii_lowercase

    plaintext = ""
    i = 0
    for c in ciphertext:
        if c in ABC:
            if i == len(keyword):
                i = 0
            if (ord(c) - ord(keyword[i]) + ord('A')) >= ord('A'):
                plaintext = plaintext + chr(ord(c) - ord(keyword[i]) + ord('A'))
            else:
                plaintext = plaintext + chr(ord(c) - ord(keyword[i]) + ord('Z') + 1)
            i += 1
        elif c in abc:
            return 'The input shouldn\'t contain lower-cased letters'
        else:
            plaintext += c
    return plaintext


print(decrypt_vigenere("LXFOPVEFRNHR", "LEMON"))


def encrypt_scytale(plaintext, circumference):
    if circumference < 0:
        return "Circumference must be a positive integer"
    ciphertext = ""
    a = [['.' for i in range(len(plaintext))]
         for j in range(circumference)]

    i = 0
    row = 0
    col = 0

    while i < len(plaintext):
        a[row][col] = plaintext[i]

        if row == (circumference - 1):
            row = 0
        else:
            row += 1

        col += 1

        i += 1

    row = 0
    col = 0

    while row < circumference:
        while col < len(plaintext):
            if a[row][col] != '.':
                ciphertext += a[row][col]
            col += 1
        row += 1
        col = 0

    return ciphertext


print(encrypt_scytale("IAMHURTVERYBADLYHELP", 5))


def decrypt_scytale(ciphertext, circumference):
    if circumference < 0:
        return "Circumference must be a positive integer"
    plaintext = ""

    a = [['.' for i in range(len(ciphertext))]
         for j in range(circumference)]

    i = 0
    row = 0
    col = 0

    while i < len(ciphertext):
        a[row][col] = '?'

        if row == (circumference - 1):
            row = 0
        else:
            row += 1
        col += 1
        i += 1

    row = 0
    col = 0
    i = 0

    while row < circumference:
        while col < len(ciphertext):
            if a[row][col] == '?':
                a[row][col] = ciphertext[i]
                i += 1
            col += 1
        row += 1
        col = 0

    i = 0
    row = 0
    col = 0

    while i < len(ciphertext):
        plaintext += a[row][col]

        if row == (circumference - 1):
            row = 0
        else:
            row += 1

        col += 1

        i += 1

    return plaintext


print(decrypt_scytale("IRYYATBHMVAEHEDLURLP", 5))


def encrypt_railfence(plaintext, num_rails):
    if num_rails < 0:
        return "Number of rails must be a positive integer"
    ciphertext = ""
    row = 0
    col = 0
    a = [['.' for i in range(len(plaintext))]
         for j in range(num_rails)]
    j = 0
    le = 1
    while j < len(plaintext):
        a[row][col] = plaintext[j]

        if le == 1 and row == (num_rails - 1):
            le = 0
        if le == 0 and row == 0:
            le = 1

        if le == 1:
            row += 1
        else:
            row -= 1
        col += 1
        j += 1

    j = 0
    i = 0
    k = 0

    while i < num_rails:
        while j < len(plaintext):
            if a[i][j] != '.':
                ciphertext += a[i][j]
                k += 1
            j += 1
        j = 0
        i += 1

    return ciphertext


print(encrypt_railfence("WEAREDISCOVEREDFLEEATONCE", 3))


def decrypt_railfence(ciphertext, num_rails):
    if num_rails < 0:
        return "Number of rails must be a positive integer"
    plaintext = ""

    row = 0
    col = 0
    a = [['.' for i in range(len(ciphertext))]
         for j in range(num_rails)]
    le = 1
    j = 0

    while j < len(ciphertext):
        a[row][col] = '?'

        if le == 1 and row == (num_rails - 1):
            le = 0
        if le == 0 and row == 0:
            le = 1

        if le == 1:
            row += 1
        else:
            row -= 1
        col += 1
        j += 1

    j = 0
    i = 0
    k = 0

    while i < num_rails:
        while j < len(ciphertext):
            if a[i][j] == '?':
                a[i][j] = ciphertext[k]
                k += 1
            j += 1
        j = 0
        i += 1

    row = 0
    col = 0
    le = 1
    j = 0

    while j < len(ciphertext):
        plaintext += a[row][col]

        if le == 1 and row == (num_rails - 1):
            le = 0
        if le == 0 and row == 0:
            le = 1

        if le == 1:
            row += 1
        else:
            row -= 1
        col += 1
        j += 1

    return plaintext


print(decrypt_railfence("WECRLTEERDSOEEFEAOCAIVDEN", 3))

def read_binary(filename):
    #filename = 'flower.jpg'
    flags = 'r'
    flags += 'b'
    with open(filename, flags) as infile:

        return infile.read()

def write_binary(output, filename):
    flags = 'w'
    flags += 'b'

    with open(filename, flags) as outfile:
        print("Writing data to {}...".format(filename))
        outfile.write(bytearray(output, 'utf-8'))

def encrypt_non_text_files(filename):
    #plain = read_binary(filename)
    ABC = string.ascii_letters

    cipher = ""

    ABC = string.ascii_letters
    punct = string.punctuation
    nr = string.digits

    dictionary = {}

    for c in ABC:
        dictionary[c] = ABC[(ABC.find(c) + 3) % 26]

    for c in punct:
        dictionary[c] = punct[(punct.find(c) + 3) % len(punct)]

    for c in nr:
        dictionary[c] = punct[(nr.find(c) + 3) % len(nr)]

    with open(filename, "rb") as file:
        data = file.read(8)
        for c in data:
            if dictionary.__contains__(c):
                cipher += dictionary[c]
            else:
                cipher += str((c+1))

    write_binary(cipher, 'cipher')

encrypt_non_text_files('b.jpeg')

########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################

def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    # Your implementation here.
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')
